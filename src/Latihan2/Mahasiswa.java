/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2;

/**
 *
 * @author anton
 */
public class Mahasiswa extends Penduduk implements Peserta {

    private String nim;

    public Mahasiswa(String nim, String nama, String tanggalLahir) {
        super(nama, tanggalLahir);
        this.nim = nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    @Override
    public double hitungIuran() {
        return Integer.parseInt(nim) / 10000;
    }

    @Override
    public String getJenisSetifikat() {
        return "Panitia";
    }

    @Override
    public String getFasilitas() {
        return "Block Note, Alat Tulis, Laptop";
    }

    @Override
    public String getKonsumsi() {
        return "Snack, Makan Siang, dan Makan Malam";
    }

}
