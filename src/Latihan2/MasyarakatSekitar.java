/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2;

/**
 *
 * @author anton
 */
public class MasyarakatSekitar extends Penduduk implements Peserta {

    private String nomor;

    public MasyarakatSekitar(String nomor, String nama, String tanggalLahir) {
        super(nama, tanggalLahir);
        this.nomor = nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }

    @Override
    public double hitungIuran() {
        return Integer.parseInt(nomor.substring(0, 2)) * 100;
    }

    @Override
    public String getJenisSetifikat() {
        return "Peserta";
    }

    @Override
    public String getFasilitas() {
        return " Block Note, Alat Tulis, dan Modul Pelatihan ";
    }

    @Override
    public String getKonsumsi() {
        return "Snack dan Makan Siang";
    }

}
