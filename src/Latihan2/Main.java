/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2;

import java.util.Scanner;

/**
 *
 * @author anton
 */
public class Main {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int jumlahPenduduk;
        Penduduk[] penduduk;
        UKM ukm;

        //Membuat objek penduduk
        jumlahPenduduk = inputInt("Masukan Jumlah Penduduk (Mahasiswa & Masyarakat): ");
        penduduk = new Penduduk[jumlahPenduduk];
        for (int i = 0; i < jumlahPenduduk; i++) {
            String nama, tanggalLahir, nomor, nim;
            System.out.println("1. Mahasiswa");
            System.out.println("2. Penduduk");
            int status = inputInt("Yang ingin anda input : ");
            switch (status) {
                case 1: {
                    nama = inputString("Masukan Nama        : ");
                    tanggalLahir = inputString("Tanggal Lahir       : ");
                    nim = inputString("NIM      : ");
                    penduduk[i] = new Mahasiswa(nim, nama, tanggalLahir);
                    break;
                }
                case 2: {
                    nama = inputString("Masukan Nama        : ");
                    tanggalLahir = inputString("Tanggal Lahir       : ");
                    nomor = inputString("Nomor      : ");
                    penduduk[i] = new MasyarakatSekitar(nomor, nama, tanggalLahir);
                    break;
                }
                default:
                    i--;
                    break;
            }
            System.out.println("");
        }
        System.out.println("\n");

        //Membuat objek ukm beserta isinya
        ukm = new UKM(inputString("Nama UKM : "));
        int status[] = new int[2];
        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                System.out.println("Silahkan Tentukan Ketua : ");
            } else if (i == 1) {
                System.out.println("Silahkan Tentukan Sekretaris : ");
            }
            System.out.printf("%-4s", "No");
            System.out.printf("%-5s", "Kode");
            System.out.printf("%-15s", "Nama");
            System.out.printf("%-15s", "NIM");
            System.out.println("");
            System.out.println("==========================================================");
            for (int j = 0; j < penduduk.length; j++) {
                if (penduduk[j] instanceof Mahasiswa && i < 1) {
                    System.out.printf("%-4s", (j + 1));
                    System.out.printf("%-5s", j);
                    System.out.printf("%-15s", ((Mahasiswa) penduduk[j]).getNama());
                    System.out.printf("%-15s", ((Mahasiswa) penduduk[j]).getNim());
                    System.out.println("");
                }
            }
            status[i] = inputInt("Pilihan anda (Kode): ");
            System.out.println("");
        }
        ukm.setKetua((Mahasiswa) penduduk[status[0]]);
        ukm.setSekretaris((Mahasiswa) penduduk[status[1]]);
        Penduduk[] pendudukTemp = new Penduduk[penduduk.length - 2];
        for (int i = 0; i < pendudukTemp.length; i++) {
            if ((!penduduk[i].equals(ukm.getKetua())) || (!penduduk[i].equals(ukm.getSekretaris()))) {
                pendudukTemp[i] = penduduk[i];
            }
        }
        ukm.setAnggota(pendudukTemp);
        pendudukTemp = null;
        System.out.println("\n\n");
        System.out.println("Nama UKM        : " + ukm.getNamaUnit());
        System.out.println("Nama Ketua      : " + ukm.getKetua().nama);
        System.out.println("Nama Sekretaris : " + ukm.getSekretaris().nama);
        System.out.println("Daftar Anggota");
        System.out.println("===========================================================================================================================================================");
        System.out.printf("%-4s", "No.");
        System.out.printf("%-15s", "Nama.");
        System.out.printf("%-15s", "Iuran");
        System.out.printf("%-15s", "Sertifikat");
        System.out.printf("%-30s", "Fasilitas");
        System.out.printf("%-30s", "Konsumsi");
        System.out.println("");
        System.out.println("===========================================================================================================================================================");
        for (int i = 0; i < ukm.getAnggota().length; i++) {
            System.out.printf("%-4s", (i + 1));
            System.out.printf("%-15s", ukm.getAnggota()[i].getNama());
            System.out.printf("%-15.2f", ukm.getAnggota()[i].hitungIuran());
            if (ukm.getAnggota()[i] instanceof Mahasiswa) {
                System.out.printf("%-15s", ((Mahasiswa) ukm.getAnggota()[i]).getJenisSetifikat());
                System.out.printf("%-30s", ((Mahasiswa) ukm.getAnggota()[i]).getFasilitas());
                System.out.printf("%-30s", ((Mahasiswa) ukm.getAnggota()[i]).getKonsumsi());
            } else if (ukm.getAnggota()[i] instanceof MasyarakatSekitar) {
                System.out.printf("%-15s", ((MasyarakatSekitar) ukm.getAnggota()[i]).getJenisSetifikat());
                System.out.printf("%-30s", ((MasyarakatSekitar) ukm.getAnggota()[i]).getFasilitas());
                System.out.printf("%-30s", ((MasyarakatSekitar) ukm.getAnggota()[i]).getKonsumsi());
            }
            System.out.println("");
        }
    }

    public static String inputString(String message) {
        String temp;
        System.out.print(message);
        temp = input.next();
        return temp;
    }

    public static int inputInt(String message) {
        int temp = 0;
        boolean valid = false;
        do {
            System.out.print(message);
            if (input.hasNextInt()) {
                temp = input.nextInt();
                valid = true;
            } else {
                System.out.print("Not a valid input, please enter again !!! \n");
                input.next();
            }
        } while (valid == false);
        return temp;
    }

    public static double inputDouble(String message) {
        double temp = 0;
        boolean valid = false;
        do {
            System.out.print(message);
            if (input.hasNextDouble()) {
                temp = input.nextDouble();
                valid = true;
            } else {
                System.out.print("Not a valid input, please enter again !!! \n");
                input.next();
            }
        } while (valid == false);
        return temp;
    }
}
